defmodule MySensors.Transport do
  @moduledoc "A behaviour for transport implementations"

  @doc "Start the transport"
  @callback start_link(network_uuid :: MySensors.uuid(), transport_config :: term()) :: GenServer.on_start()
end
