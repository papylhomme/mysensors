defmodule MySensors.Gateway do
  alias MySensors.Node
  alias MySensors.PubSub
  alias MySensors.Network
  alias MySensors.Message

  @moduledoc "A server for routing and gateway features"

  @doc false
  use GenServer
  require Logger


  # Default timeout when waiting for a reply from the network
  @ack_timeout 1000

  # Topic used for gateway logs
  @gwlogs_topic "gwlogs"


  #########
  #  API
  #########

  @doc "Start the gateway server"
  @spec start_link(MySensors.uuid()) :: GenServer.on_start
  def start_link(network_uuid), do: GenServer.start_link(__MODULE__, network_uuid)

  @doc "Send a message to the MySensors network"
  @spec send_message(pid, Message.t()) :: :ok | {:error, term}
  def send_message(server, message), do: :ok = GenServer.call(server, {:send_message, message})

  @doc "Request gateway version"
  @spec request_version(pid) :: {:ok, String.t()} | {:error, :timeout}
  def request_version(server) do
    info = GenServer.call(server, :info)
    _request_version(server, info.uuid)
  end

  @doc """
  Subscribe to the gwlogs topic

  Caller process will receive messages `{:mysensors, :gwlogs, payload}` where
  payload is of type `t:String.t/0`
  """
  @spec subscribe_gwlogs(MySensors.uuid()) :: :ok | {:error, term()}
  def subscribe_gwlogs(network_uuid), do: PubSub.subscribe(network_uuid, @gwlogs_topic)

  @doc "Unubscribe from the gwlogs topic"
  @spec unsubscribe_gwlogs(MySensors.uuid()) :: :ok | {:error, term()}
  def unsubscribe_gwlogs(network_uuid), do: PubSub.unsubscribe(network_uuid, @gwlogs_topic)

  # Broadcast a gwlogs `event` to the gwlogs topic
  @doc false
  @spec broadcast_gwlogs(PubSub.name(), String.t()) :: :ok | {:error, term()}
  def broadcast_gwlogs(bus, event),
    do: PubSub.broadcast(bus, @gwlogs_topic, {:mysensors, :gwlogs, event})



  ####################
  #  Implementation
  ####################

  # INIT: start the presentation manager
  @doc false
  def init(network_uuid) when is_binary(network_uuid) do
    Network.subscribe_incoming(network_uuid)

    {:ok, %{uuid: network_uuid, bus: PubSub.bus_name(network_uuid)}}
  end

  # CALL: Returns information for `request_version`
  def handle_call(:info, _from, state), do: {:reply, state, state}

  # CALL: Send a message to the network
  def handle_call({:send_message, message}, _from, state) do
    _send_message(state, message)
    {:reply, :ok, state}
  end

  # INFO: Handle incoming messages
  def handle_info({:mysensors, :incoming, message}, state) do
    _process_incoming(message, state)
  end


  ##############
  #  Internals
  ##############

  # Send a message to the network
  defp _send_message(state, message) do
    Logger.debug(fn -> "Sending message #{message}" end)
    Network.broadcast_outgoing(state.bus, message)
  end

  # Start a task that sends a version request message and wait for the reply
  defp _request_version(gateway, network_uuid) do
    task =
      Task.async(fn ->
        Network.subscribe_incoming(network_uuid)
        :ok = send_message(gateway, Message.new(0, 255, :internal, I_VERSION))

        receive do
          {:mysensors, :incoming,
           %{
             child_sensor_id: 255,
             command: :internal,
             node_id: 0,
             type: I_VERSION,
             payload: version
           }} ->
            version
        end
      end)

    case Task.yield(task, @ack_timeout) || Task.shutdown(task) do
      {:ok, result} -> {:ok, result}
      nil -> {:error, :timeout}
    end
  end

  # Forward log messages
  defp _process_incoming(%{command: :internal, type: I_LOG_MESSAGE, payload: log}, state) do
    broadcast_gwlogs(state.bus, log)
    {:noreply, state}
  end

  # Handle time requests
  defp _process_incoming(msg = %{command: :internal, type: I_TIME}, state) do
    Logger.debug("Received time request: #{msg}")

    _send_message(
      state,
      Message.new(
        msg.node_id,
        msg.child_sensor_id,
        :internal,
        I_TIME,
        DateTime.utc_now() |> DateTime.to_unix(:second)
      )
    )

    {:noreply, state}
  end

  # Handle config requests
  defp _process_incoming(msg = %{command: :internal, type: I_CONFIG}, state) do
    Logger.debug("Received configuration request: #{msg}")

    payload =
      case Application.get_env(:mysensors, :measure, :metric) do
        :metric -> "M"
        :imperial -> "I"
      end

    _send_message(
      state,
      Message.new(
        msg.node_id,
        msg.child_sensor_id,
        :internal,
        I_CONFIG,
        payload
      )
    )

    {:noreply, state}
  end

  # Forward internal messages to node
  defp _process_incoming(
         msg = %{command: :internal, child_sensor_id: 255},
         state
       ) do
    Node.broadcast_node_message(state.bus, Node.uuid(state.uuid, msg.node_id), msg)
    {:noreply, state}
  end

    # Forward requests to nodes
  defp _process_incoming(msg = %{command: c}, state)
       when c in [:req, :set] do
    Node.broadcast_node_message(state.bus, Node.uuid(state.uuid, msg.node_id), msg)
    {:noreply, state}
  end



  # Discard remaining MySensors messages
  defp _process_incoming(_message, state) do
    {:noreply, state}
  end

end
