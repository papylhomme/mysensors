defmodule MySensors.Storage.DETS do
  @moduledoc false
  @behaviour MySensors.Storage


  @autosave_delay 5 * 60 * 1000


  @impl true
  def start_link(network_uuid) do
    nodes_db =
      Application.get_env(:mysensors, :data_dir, "./")
      |> Path.join("network_#{network_uuid}.db")
      |> String.to_charlist()

    {:ok, _tid} = :dets.open_file(nodes_db, ram_file: true, auto_save: @autosave_delay)
  end


  @impl true
  def node_known?(storage, node_id), do: :dets.match(storage, {:_, node_id, :_}) != []


  @impl true
  def each_nodes(storage, fun) do
    :dets.match(storage, :'$1')
    |> Enum.each(fn [item] -> fun.(item) end)
  end


  @impl true
  def map_nodes(storage, fun) do
    :dets.match(storage, :'$1')
    |> Enum.map(fn [item] -> fun.(item) end)
  end


  @impl true
  def get_node(storage, node_uuid) do
    [{_uuid, _id, specs}] = :dets.lookup(storage, node_uuid)
    specs
  end


  @impl true
  def insert_node(storage, node_uuid, node_specs), do: :ok = :dets.insert(storage, {node_uuid, node_specs.node_id, node_specs})


  @impl true
  def remove_node(storage, node_uuid), do: :ok = :dets.delete(storage, node_uuid)

end
