defmodule MySensors.Node do
  alias MySensors.Types
  alias MySensors.Sensor
  alias MySensors.PubSub
  alias MySensors.Network
  alias MySensors.MessageQueue

  alias __MODULE__
  alias __MODULE__.NodeUpdatedEvent


  # TODO doc and specs for node properties last_seen, battery, status, ...

  @moduledoc """
  A server to interract with a MySensors node

  This server handle messages from the network to update the node's information (available via `info/1`) and
  sensors (available via `sensors/1`). It is also responsible for keeping the `MySensors.Sensor` servers in sync
  with the network.

  Each node contains it's own `MySensors.MessageQueue` to process commands sent using `command/5`. For sleeping nodes,
  it allows for queuing messages until the node wakes up.

  Some node properties are also updated dynamically and notified using a `MySensors.Node.NodeUpdatedEvent`, as `battery`, `status` and `last_seen`.

  Finally the server also handle spec updates from the MySensors node and can restart itself when its sensors have been updated in an incompatible way.
  """

  @doc false
  use GenServer
  require Logger

  defstruct uuid: nil,
            node_id: nil,
            type: nil,
            version: nil,
            sketch_name: nil,
            sketch_version: nil,
            status: :unknown,
            battery: nil,
            last_seen: nil,
            sensors: %{}

  @typedoc "Sensors info"
  @type sensors :: %{optional(Types.id()) => pid}

  @typedoc "Node's info"
  @type t :: %__MODULE__{
          uuid: MySensors.uuid(),
          node_id: Types.id(),
          type: String.t(),
          version: String.t(),
          sketch_name: String.t(),
          sketch_version: String.t(),
          sensors: sensors
        }

  # PubSub topic
  @nodes_events_topic "nodes_events"
  defp _node_events_topic(node), do: "node_#{node}_events"
  defp _node_commands_topic(node), do: "node_#{node}_commands"
  defp _node_messages_topic(node), do: "node_#{node}_messages"


  #########
  #  API
  #########

  @doc false
  @spec uuid(MySensors.uuid(), Types.id) :: MySensors.uuid()
  # Generate an UUID for the given node
  def uuid(network_uuid, node_id), do: UUID.uuid5(network_uuid, "#{node_id}")

  @doc "Start a node server"
  @spec start_link({MySensors.uuid(), pid(), reference, MySensors.uuid()}) :: GenServer.on_start()
  def start_link({network_uuid, storage, node_uuid}),
    do: GenServer.start_link(__MODULE__, {network_uuid, storage, node_uuid})

  @doc "Request information about the node"
  @spec info(pid) :: t
  def info(pid), do: GenServer.call(pid, :info)

  @doc "List the sensors"
  @spec sensors(pid) :: [MySensors.uuid()]
  def sensors(pid), do: GenServer.call(pid, :list_sensors)

  @doc "Get the node's message queue"
  @spec queue(pid) :: pid
  def queue(pid), do: GenServer.call(pid, :queue)

  @doc false
  @spec update_specs(pid, t) :: :ok
  # Handle a specs updated event
  def update_specs(pid, specs), do: GenServer.cast(pid, {:update_specs, specs})

  @doc "Send a command to the node"
  @spec command(pid, Types.id(), Types.command(), Types.type(), String.t()) :: :ok
  def command(pid, sensor_id, command, type, payload \\ ""),
    do: GenServer.cast(pid, {:node_command, sensor_id, command, type, payload})


  ############
  #  Events
  ############

  defmodule NodeDiscoveredEvent do
    @moduledoc "An event generated when a new node is discovered"
    defstruct uuid: nil, specs: nil

    @typedoc "The event struct"
    @type t :: %__MODULE__{uuid: MySensors.uuid(), specs: Node.t()}

    @doc false
    @spec broadcast(pid(), Node.t()) :: t
    # Create and broadcast a NodeDiscoveredEvent
    def broadcast(bus, specs), do: Node.broadcast_node_event(bus, specs.uuid, %__MODULE__{uuid: specs.uuid, specs: specs})
  end

  defmodule NodeUpdatedEvent do
    @moduledoc """
    An event generated when a node is updated

    The `info` field can contain any fields from `t:MySensors.Node.t/0`
    """
    defstruct uuid: nil, info: %{}

    @typedoc "The event struct"
    @type t :: %__MODULE__{uuid: MySensors.uuid(), info: map}

    @doc false
    @spec broadcast(pid(), MySensors.uuid(), map) :: t
    # Create and broadcast a NodeUpdatedEvent
    def broadcast(bus, uuid, info), do: Node.broadcast_node_event(bus, uuid, %__MODULE__{uuid: uuid, info: info})
  end


  #
  # Node events topic
  #

  @doc """
  Subscribe to the global nodes events topic

  Caller process will receive messages `{:mysensors, :node_event, payload}`, where
  payload is one of `t:MySensors.Node.NodeDiscoveredEvent.t/0` or `t:MySensors.Node.NodeUpdatedEvent.t/0`
  """
  @spec subscribe_nodes_events(MySensors.uuid()) :: :ok | {:error, term()}
  def subscribe_nodes_events(network_uuid), do: PubSub.subscribe(network_uuid, @nodes_events_topic)

  @doc "Unubscribe from the global nodes events topic"
  @spec unsubscribe_nodes_events(MySensors.uuid()) :: :ok | {:error, term()}
  def unsubscribe_nodes_events(network_uuid), do: PubSub.unsubscribe(network_uuid, @nodes_events_topic)

  @doc """
  Subscribe to the given node event topic

  Caller process will receive messages `{:mysensors, :node_event, payload}`, where
  payload is one of `t:MySensors.Node.NodeDiscoveredEvent.t/0` or `t:MySensors.Node.NodeUpdatedEvent.t/0`
  """
  @spec subscribe_node_events(MySensors.uuid(), MySensors.uuid()) :: :ok | {:error, term()}
  def subscribe_node_events(network_uuid, node_uuid), do: PubSub.subscribe(network_uuid, _node_events_topic(node_uuid))

  @doc "Unubscribe from the given node event topic"
  @spec unsubscribe_node_events(MySensors.uuid(), MySensors.uuid()) :: :ok | {:error, term()}
  def unsubscribe_node_events(network_uuid, node_uuid), do: PubSub.unsubscribe(network_uuid, _node_events_topic(node_uuid))

  # Broadcast a node `event` to the node events topics
  @doc false
  @spec broadcast_node_event(PubSub.name(), MySensors.uuid(), term()) :: :ok | {:error, term()}
  def broadcast_node_event(bus, node_uuid, event) do
    PubSub.broadcast(bus, _node_events_topic(node_uuid), {:mysensors, :node_event, event})
    PubSub.broadcast(bus, @nodes_events_topic, {:mysensors, :node_event, event})
  end


  #
  # Node commands topic
  #

  @doc """
  Subscribe to the given node command topic

  Caller process will receive messages `{:mysensors, :node_command, payload}`
  """
  @spec subscribe_node_commands(MySensors.uuid(), MySensors.uuid()) :: :ok | {:error, term()}
  def subscribe_node_commands(network_uuid, node_uuid), do: PubSub.subscribe(network_uuid, _node_commands_topic(node_uuid))

  @doc "Unubscribe from the given node command topic"
  @spec unsubscribe_node_commands(MySensors.uuid(), MySensors.uuid()) :: :ok | {:error, term()}
  def unsubscribe_node_commands(network_uuid, node_uuid), do: PubSub.unsubscribe(network_uuid, _node_commands_topic(node_uuid))

  # Broadcast a node command `event`
  @doc false
  @spec broadcast_node_command(PubSub.name(), MySensors.uuid(), term()) :: :ok | {:error, term()}
  def broadcast_node_command(bus, node_uuid, event), do: PubSub.broadcast(bus, _node_commands_topic(node_uuid), {:mysensors, :node_command, event})


  #
  # Node messages topic
  #

  @doc """
  Subscribe to the given node messages topic

  Caller process will receive messages `{:mysensors, :message, payload}` where
  payload is of type `t:MySensors.Message.t/0`
  """
  @spec subscribe_node_messages(MySensors.uuid(), MySensors.uuid()) :: :ok | {:error, term()}
  def subscribe_node_messages(network_uuid, node_uuid), do: PubSub.subscribe(network_uuid, _node_messages_topic(node_uuid))

  @doc "Unubscribe from the given node messages topic"
  @spec unsubscribe_node_messages(MySensors.uuid(), MySensors.uuid()) :: :ok | {:error, term()}
  def unsubscribe_node_messages(network_uuid, node_uuid), do: PubSub.unsubscribe(network_uuid, _node_messages_topic(node_uuid))

  # Broadcast a node `message`
  @doc false
  @spec broadcast_node_message(PubSub.name(), MySensors.uuid(), MySensors.Message.t()) :: :ok | {:error, term()}
  def broadcast_node_message(bus, node_uuid, message), do: PubSub.broadcast(bus, _node_messages_topic(node_uuid), {:mysensors, :message, message})


  #############################
  #  GenServer implementation
  #############################

  # Initialize the server
  @doc false
  def init({network_uuid, storage, node_uuid}) do
    bus = PubSub.bus_name(network_uuid)
    subscribe_node_messages(network_uuid, node_uuid)
    {:ok, supervisor} = Supervisor.start_link([], strategy: :one_for_one)

    # Init message queue
    {:ok, queue} =
      MessageQueue.start_link(network_uuid, %{
        node_uuid: node_uuid, 
        handler: fn event -> broadcast_node_command(bus, node_uuid, event) end
      })

    # Load specs from storage and init info
    node_specs = MySensors.Storage.get_node(storage, node_uuid)

    info = %__MODULE__{
      uuid: node_uuid,
      node_id: node_specs.node_id,
      type: node_specs.type,
      version: node_specs.version,
      sketch_name: node_specs.sketch_name,
      sketch_version: node_specs.sketch_version,
      sensors: []
    }

    Logger.info("New node #{node_specs.node_id} online (#{inspect(node_specs.sensors)})")

    # Register self
    Network.broadcast_registration(bus, {:node, self(), info})

    {:ok,
      %{
        bus: bus,
        info: info,
        supervisor: supervisor,
        network_uuid: network_uuid,
        message_queue: queue
      },
      {:continue, {:start_sensors, node_specs}}
    }
  end

  # CONTINUE: start sensors
  @doc false
  def handle_continue({:start_sensors, node_specs}, state) do
    sensors =
      for {sensor_id, sensor_specs} <- node_specs.sensors do
        sensor_uuid = _sensor_uuid(state.info.uuid, sensor_id)
        params = {state.network_uuid, self(), state.info.uuid, sensor_uuid, sensor_specs}
        child_spec = Supervisor.child_spec({Sensor, params}, id: sensor_uuid)
        {:ok, _pid} = Supervisor.start_child(state.supervisor, child_spec)

        sensor_uuid
      end

    {:noreply, %{state | info: %{state.info | sensors: sensors}}}
  end

  # Handle info call
  def handle_call(:info, _from, state) do
    {:reply, state.info, state}
  end

  # Handle queue call
  def handle_call(:queue, _from, state) do
    {:reply, state.message_queue, state}
  end

  # Handle list_sensors call
  def handle_call(:list_sensors, _from, state) do
    {:reply, state.info.sensors, state}
  end

  # Handle node specs update
  # TODO more robust change detection
  def handle_cast({:update_specs, node_specs}, state = %{info: info}) do
    if length(info.sensors) == map_size(node_specs.sensors) do
      updated_info = %{
        type: node_specs.type,
        version: node_specs.version,
        sketch_name: node_specs.sketch_name,
        sketch_version: node_specs.sketch_version,
        last_seen: DateTime.utc_now(),
        sensors: Map.keys(node_specs.sensors)
      }

      Logger.info("Node #{info.node_id} received a specs update")
      NodeUpdatedEvent.broadcast(state.bus, info.uuid, updated_info)

      {:noreply, put_in(state[:info], Map.merge(info, updated_info))}
    else
      Logger.warning("Node #{info.node_id} received incompatible specs update, restarting")
      # TODO broadcast events to notify the change
      {:stop, {:shutdown, :specs_updated}, state}
    end
  end

  # Handle node commands
  def handle_cast({:node_command, sensor_id, command, type, payload}, state) do
    MessageQueue.push(
      state.message_queue,
      MySensors.Message.new(state.info.node_id, sensor_id, command, type, payload)
    )

    {:noreply, state}
  end

  #######################
  #  Messages handlers
  #######################

  # INFO: Handle internal commands
  def handle_info(
        {:mysensors, :message, msg = %{command: :internal, child_sensor_id: 255}},
        state
      ) do
    info = state.info

    updated_info =
      case msg.type do
        # handle battery level
        I_BATTERY_LEVEL ->
          Logger.debug("Node #{info.node_id} handling battery level: #{msg.payload}")

          NodeUpdatedEvent.broadcast(state.bus, info.uuid, %{
            battery: msg.payload,
            last_seen: DateTime.utc_now()
          })

          %{info | battery: msg.payload}

        # handle pre sleep notification
        I_PRE_SLEEP_NOTIFICATION ->
          Logger.debug("Node #{info.node_id} handling pre sleep")

          NodeUpdatedEvent.broadcast(state.bus, info.uuid, %{
            status: "SLEEPING",
            last_seen: DateTime.utc_now()
          })

          %{info | status: "SLEEPING", last_seen: DateTime.utc_now()}

        # handle post sleep notification
        I_POST_SLEEP_NOTIFICATION ->
          Logger.debug("Node #{info.node_id} waking up")

          # flush queued message
          MessageQueue.flush(state.message_queue)

          # update status
          NodeUpdatedEvent.broadcast(state.bus, info.uuid, %{
            status: "RUNNING",
            last_seen: DateTime.utc_now()
          })

          %{info | status: "RUNNING", last_seen: DateTime.utc_now()}

        # discard other commands
        _ ->
          info
      end

    {:noreply, %{state | info: updated_info}}
  end

  # INFO: Handle incoming sensor messages
  def handle_info({:mysensors, :message, msg = %{child_sensor_id: sensor_id}}, state) when sensor_id != 255 do
    sensor_uuid = _sensor_uuid(state.info.uuid, msg.child_sensor_id)
    Sensor.broadcast_sensor_message(state.bus, sensor_uuid, msg)
    {:noreply, state}
  end

  # INFO: Handle unexpected messages
  def handle_info(msg, state) do
    Logger.warning("Node #{state.info.node_id} handling unexpected message #{inspect(msg)}")
    {:noreply, state}
  end

  #############
  #  Internals
  #############

  # Generate an UUID for the given sensor
  defp _sensor_uuid(node_uuid, sensor_id), do: UUID.uuid5(node_uuid, "#{sensor_id}")

end
