defmodule MySensors.Storage do

  @moduledoc "Behaviour for storage API"

  @typedoc "Storage type"
  @type t :: {module(), identifier()}


  #############
  # Callbacks
  #############

  @doc """
  Start the storage for the given `network_uuid`

  This callback's result will be used as identifier when calling
  the other functions
  """
  @callback start_link(MySensors.uuid()) :: {:ok, identifier()}


  @doc """
  Test whether `node_id` is known in the system

  *NOTE*: the lookup parameter is the MySensors node id, not its internal UUID
  """
  @callback node_known?(identifier(), MySensors.Types.id()) :: boolean


  @doc "Call `fun` for each existing nodes"
  @callback each_nodes(identifier(), (MySensors.uuid() -> term())) :: term()


  @doc "Map the nodes using `fun`"
  @callback map_nodes(identifier(), (MySensors.uuid() -> term())) :: [term()]


  @doc "Get the specs for `node_uuid`"
  @callback get_node(identifier(), MySensors.uuid()) :: MySensors.Node.t() | nil


  @doc "Insert or update a node in the storage"
  @callback insert_node(identifier(), MySensors.uuid(), MySensors.Node.t()) :: :ok


  @doc "Remove a node from storage"
  @callback remove_node(identifier(), MySensors.uuid()) :: :ok


  ####################
  # Dispatch methods
  ####################

  @doc "Calls `c:start_link/1` on the given implementation"
  @spec start_link(module(), MySensors.uuid()) :: {:ok, t()}
  def start_link(impl \\ MySensors.Storage.DETS, network_uuid) do
    {:ok, storage} = impl.start_link(network_uuid)
    {:ok, {impl, storage}}
  end


  @doc "Calls `c:node_known?/2` on the given implementation"
  @spec node_known?(t(), MySensors.Types.id()) :: boolean
  def node_known?({impl, storage} = _storage, node_id), do: impl.node_known?(storage, node_id)


  @doc "Calls `c:each_nodes/2` on the given implementation"
  @spec each_nodes(t(), (MySensors.uuid() -> term())) :: term()
  def each_nodes({impl, storage} = _storage, fun), do: impl.each_nodes(storage, fun)


  @doc "Calls `c:map_nodes/2` on the given implementation"
  @spec map_nodes(t(), (MySensors.uuid() -> term())) :: term()
  def map_nodes({impl, storage} = _storage, fun), do: impl.map_nodes(storage, fun)


  @doc "Calls `c:get_node/2` on the given implementation"
  @spec get_node(t(), MySensors.uuid()) :: MySensors.Node.t() | nil
  def get_node({impl, storage} = _storage, node_uuid), do: impl.get_node(storage, node_uuid)


  @doc "Calls `c:insert_node/3` on the given implementation"
  @spec insert_node(t(), MySensors.uuid(), MySensors.Node.t()) :: :ok
  def insert_node({impl, storage} = _storage, node_uuid, node_specs), do: impl.insert_node(storage, node_uuid, node_specs)


  @doc "Calls `c:remove_node/2` on the given implementation"
  @spec remove_node(t(), MySensors.uuid()) :: :ok
  def remove_node({impl, storage} = _storage, node_uuid), do: impl.remove_node(storage, node_uuid)

end
