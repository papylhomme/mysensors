defmodule GatewayTest do
  use ExUnit.Case, async: true

  alias MySensors.Message
  alias MySensors.Network
  alias MySensors.Gateway
  alias MySensors.Node


  setup_all do
		uuid = UUID.uuid4()
    {:ok, bus} = MySensors.PubSub.start_link(uuid)
		{:ok, gateway} = Gateway.start_link(uuid)
		{:ok, %{uuid: uuid, bus: bus, gateway: gateway}}
  end


  test "redirect gateway logs", ctx do
    Gateway.subscribe_gwlogs(ctx.uuid)
    msg = Message.new(0, 0, :internal, I_LOG_MESSAGE, "payload")
    Network.broadcast_incoming(ctx.bus, msg)
    assert_receive({:mysensors, :gwlogs, "payload"})
  end


  test "send message", ctx do
    msg = Message.new(0, 0, :req, V_STATUS)
    Network.subscribe_outgoing(ctx.uuid)
    Gateway.send_message(ctx.gateway, msg)
    assert_receive({:mysensors, :outgoing, ^msg})
  end


  test "request version", ctx do
    Network.subscribe_outgoing(ctx.uuid)
    task = Task.async(fn -> Gateway.request_version(ctx.gateway) end)
    assert_receive({:mysensors, :outgoing, %Message{node_id: 0, child_sensor_id: 255, command: :internal, type: I_VERSION}})
    Network.broadcast_incoming(ctx.bus, Message.new(0, 255, :internal, I_VERSION, "test_version"))
    assert {:ok, "test_version"} == Task.await(task)
  end


  test "redirect node messages", ctx do
    Node.subscribe_node_messages(ctx.uuid, Node.uuid(ctx.uuid, 0))
    Network.broadcast_incoming(ctx.bus, Message.new(0, 1, :req, V_STATUS))
    assert_receive({:mysensors, :message, %Message{node_id: 0, child_sensor_id: 1, command: :req, type: V_STATUS}})
  end


  test "request config", ctx do
    Network.subscribe_outgoing(ctx.uuid)
    Network.broadcast_incoming(ctx.bus, Message.new(0, 255, :internal, I_CONFIG))
    assert_receive({:mysensors, :outgoing, %Message{node_id: 0, child_sensor_id: 255, command: :internal, type: I_CONFIG, payload: "M"}})
  end


  test "request time", ctx do
    Network.subscribe_outgoing(ctx.uuid)
    Network.broadcast_incoming(ctx.bus, Message.new(0, 255, :internal, I_TIME))
    assert_receive({:mysensors, :outgoing, %Message{node_id: 0, child_sensor_id: 255, command: :internal, type: I_TIME, payload: time}})

    current_time = DateTime.utc_now() |> DateTime.to_unix(:second)
    assert current_time - time < 5
  end


end
